import React from 'react';
import { useEffect } from 'react';
import { Image, Text, View } from 'react-native';
import Login from '../Login/index'
const Splash=({navigation})=>{
    useEffect(()=>{
        setTimeout(function(){
                navigation.navigate('Login')
          }, 2000);
})
    return(
        <View style={{backgroundColor:'#ffcc00', height:'100%',width:'100%'}}>
            <View style={{justifyContent:'center', alignItems:'center', marginTop:'40%'}}>
                <Image source={require('../../assets/taxi.png')} resizeMode="center" style={{height:'50%',width:'50%',
                 borderRadius:400}}></Image>
                 <Text style={{fontSize:20, color:'#5e0808', fontWeight:'bold'}}>TakeOff</Text>
            </View>
        </View>
    )
}
export default Splash;