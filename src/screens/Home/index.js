import React, { useEffect, useState } from 'react'
import { View, Text, Alert, TextInput, TouchableOpacity, Image, ScrollView, KeyboardAvoidingView } from 'react-native'
import Geolocation from '@react-native-community/geolocation';
import MapView, { Marker } from 'react-native-maps';
import Intercity from '../Intercity';
import { useSafeArea } from 'react-native-safe-area-context';

const Home = ({navigation}) => {
  // const [satte,setState]=useState('')
  const [state, setState] = useState({
    region: {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0.97,
      longitudeDelta: 0.98,
    }
  })

  useEffect(() => {
    Geolocation.getCurrentPosition(
      position => {
        setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.97,
            longitudeDelta: 0.98,
          }
        });
      },
      (error) => {
        Alert.alert(error.message.toString());
      },
      {
        showLocationDialog: true,
        enableHighAccuracy: true,
        timeout: 20000
      },
    );

  })

  const Intercity=()=>{
    navigation.navigate('Intercity')
  }
  return (
    <View style={{ flex: 1, height: '50%', backgroundColor: '#ffe0cc' }}>
      <MapView
        style={{ height: '50%' }}
        showsUserLocation={true}
        region={state.region}
        onRegionChange={() => state.region}
      >
        <Marker
          coordinate={state.region}
        />
      </MapView>
      <View>
        <View style={{
          flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginLeft: '3%', padding: 15
        }}>
          <TouchableOpacity>
            <View style={{ borderWidth: 2, padding: 10, backgroundColor: '#ff9999', borderRadius: 10 }}>
              <Image source={require('../../assets/car.png')} style={{ height: 30, width: 40 }}></Image>
              <Text>Car</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={{ borderWidth: 2, padding: 10, backgroundColor: '#ff9999', borderRadius: 10 }}>
              <Image source={require('../../assets/motorbike.png')} style={{ height: 30, width: 40 }}></Image>
              <Text>Moto</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>Intercity()}>
            <View style={{ borderWidth: 2, padding: 10, backgroundColor: '#ff9999', borderRadius: 10 }}>
              <Image source={require('../../assets/distance.png')} style={{ height: 30, width: 40 }}></Image>
              <Text>Intercity</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ height: '30%', padding: 30 }}>
          <ScrollView>
            <View style={{ borderBottomWidth: 2, flexDirection:'row', justifyContent:'flex-start', alignItems:'center' }}>
            <Image source={require('../../assets/pickup.png')} style={{ height: 25, width: 25 }}></Image>
            <View style={{marginLeft:'3%'}}>
              <TextInput placeholder='Pickup location'></TextInput>
              </View>
            </View>
            <View style={{ borderBottomWidth: 2,flexDirection:'row', justifyContent:'flex-start', alignItems:'center'  }}>
            <Image source={require('../../assets/destination.png')} style={{ height: 25, width: 25 }}></Image>
              <View style={{marginLeft:'3%'}}>
              <TextInput placeholder='Destination'></TextInput>
              </View>
            </View>
            <View style={{ borderBottomWidth: 2, flexDirection:'row', justifyContent:'flex-start', alignItems:'center' }}>
              <Image source={require('../../assets/rupee.png')} style={{ height: 25, width: 25 }}></Image>
              <View style={{marginLeft:'3%'}}>
              <TextInput placeholder='Offer your fare'></TextInput>
              </View>
            </View>
            <View style={{ borderBottomWidth: 2, flexDirection:'row', justifyContent:'flex-start', alignItems:'center'}}>
            <Image source={require('../../assets/message.png')} style={{ height: 25, width: 35 }}></Image>
            <View style={{marginLeft:'3%'}}>
              <TextInput placeholder='Comment and wishes'></TextInput>
              </View>
            </View>
          </ScrollView>
        </View>
        <TouchableOpacity>
          <View style={{
            justifyContent: 'center', alignItems: "center",
            borderWidth: 2, marginLeft: '25%', marginRight: '25%', borderRadius: 10,
            backgroundColor: '#ff9999', padding: 6, marginTop: '10%'
          }}>
            <Text style={{ fontSize: 20, color: 'black' }}>Book a ride</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}
export default Home;