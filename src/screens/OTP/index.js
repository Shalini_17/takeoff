import React, { useState } from 'react';
import { useRef } from 'react';

import { View, TextInput, Text, TouchableOpacity, OTPTextInput } from 'react-native';
const Otp = ({ navigation }) => {
    const [otp1, setotp1] = useState("")
    const [otp2, setotp2] = useState("")
    const [otp3, setotp3] = useState("")
    const [otp4, setotp4] = useState("")
    const [otp5, setotp5] = useState("")
    const ref_input1 = useRef();
    const ref_input2 = useRef();
    const ref_input3 = useRef();
    const ref_input4 = useRef();
    const ref_input5 = useRef();

    const Verify = () => {
        navigation.navigate('Home')
    }
    return (
        <View style={{ backgroundColor: '#ffe0cc', height: '100%', width: '100%' }}>
            <View style={{
                flexDirection: 'row', justifyContent: 'space-evenly',
                alignItems: 'center', marginTop: '50%', marginLeft: '20%', marginRight: '20%'
            }}>
                <View style={{ borderBottomWidth: 1 }}>
                    <TextInput placeholder=' ' returnKeyType="next" keyboardType="numeric" maxLength={1} ref={ref_input1}
                        onChangeText={otp1 => {
                            setotp1(otp1)
                            if (otp1) ref_input2.current.focus();
                        }}></TextInput>
                </View>
                <View style={{ borderBottomWidth: 1 }}>
                    <TextInput placeholder=' ' returnKeyType="next" keyboardType="numeric" maxLength={1} ref={ref_input2}
                        onChangeText={otp2 => {
                            setotp2(otp2)
                            if (otp2) ref_input3.current.focus();
                        }}></TextInput>
                </View>

                <View style={{ borderBottomWidth: 1 }}>
                    <TextInput placeholder=' ' returnKeyType="next" keyboardType="numeric" maxLength={1} ref={ref_input3}
                        onChangeText={otp3 => {
                            setotp3(otp3)
                            if (otp3) ref_input4.current.focus();
                        }}></TextInput>
                </View>
                <View style={{ borderBottomWidth: 1 }}>
                    <TextInput placeholder=' ' returnKeyType="next" keyboardType="numeric" maxLength={1} ref={ref_input4}
                        onChangeText={otp4 => {
                            setotp4(otp4)
                            if (otp4) ref_input5.current.focus();
                        }}></TextInput>
                </View>
                <View style={{ borderBottomWidth: 1 }}>
                    <TextInput placeholder=' ' returnKeyType="next" keyboardType="numeric" maxLength={1} ref={ref_input5}
                        onChangeText={otp5 => {

                            setotp5(otp5)
                            if (otp5) ref_input5.current.focus();
                        }}></TextInput>
                </View>

            </View>
            <TouchableOpacity onPress={() => Verify()} >
                <View style={{
                    marginTop: '10%', justifyContent: 'center', alignItems: "center",
                    borderWidth: 2, marginLeft: '40%', marginRight: '40%', borderRadius: 10,
                    backgroundColor: '#ff9999', padding: 6
                }}>
                    <Text style={{ fontSize: 20, color: 'black' }}>Verify</Text>
                </View>

            </TouchableOpacity>
        </View>
    )
}
export default Otp;