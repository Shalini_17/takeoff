import * as React from 'react';
import { View, useWindowDimensions, Text, TextInput, TouchableOpacity, ScrollView, Image } from 'react-native';

import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

const FirstRoute = () => (
  <View style={{ flex: 1, backgroundColor: '#ffe0cc', height:'100%', width:'100%'}}> 
  <ScrollView>
  <View>
     
      <View style={{
        borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
        marginRight: '10%', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'
      }}>
        <Image source={require('../../assets/pickup.png')} style={{ height: 25, width: 25 }}></Image>
        <View style={{ marginLeft: '3%' }}>
        <TextInput placeholder='PICKUP LOCATION'></TextInput>
        </View>
      </View>
      <View style={{
        borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
        marginRight: '10%', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'
      }}>
        <Image source={require('../../assets/cityscape.jpg')} style={{ height: 25, width: 25 }}></Image>
      <View style={{ marginLeft: '3%' }}>

        <TextInput placeholder='Select your city'></TextInput>
        </View>
      </View>
      <View style={{
        borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
        marginRight: '10%', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'
      }}>
         <Image source={require('../../assets/left-arrow.png')} style={{ height: 25, width: 25 }}></Image>
      <View style={{ marginLeft: '3%' }}>
        <TextInput placeholder='Address or area'></TextInput>
        </View>
      </View>
      <View style={{
        borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
        marginRight: '10%', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'
      }}>
         <Image source={require('../../assets/destination.png')} style={{ height: 25, width: 25 }}></Image>
              <View style={{marginLeft:'3%'}}>
        <TextInput placeholder='DESTINATION'></TextInput>
        </View>
      </View>
      <View style={{
        borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
        marginRight: '10%', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'
      }}>
         <Image source={require('../../assets/cityscape.jpg')} style={{ height: 25, width: 25 }}></Image>
      <View style={{ marginLeft: '3%' }}>
        <TextInput placeholder='To city'></TextInput>
        </View>
      </View>
      <View style={{
        borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
        marginRight: '10%', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'
      }}>
        <Image source={require('../../assets/right-arrow.png')} style={{ height: 25, width: 25 }}></Image>
      <View style={{ marginLeft: '3%' }}>
        <TextInput placeholder='Address or area'></TextInput>
        </View>
      </View>
      <View style={{
        borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
        marginRight: '10%', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'
      }}>
         <Image source={require('../../assets/rupee.png')} style={{ height: 25, width: 25 }}></Image>
              <View style={{marginLeft:'3%'}}>
        <TextInput placeholder='Fare'></TextInput>
        </View>
      </View>
      <View style={{
        borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
        marginRight: '10%', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'
      }}>
         <Image source={require('../../assets/notes5.png')} style={{ height: 25, width: 25 }}></Image>
        <View style={{marginLeft:'3%'}}>
        <TextInput placeholder='Notes'></TextInput>
        </View>
      </View>
      <View style={{
        borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
        marginRight: '10%',
        flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'
      }}>
        <Image source={require('../../assets/calendar.png')} style={{ height: 25, width: 25 }}></Image>
        <View style={{ marginLeft: '3%' }}>
          <TextInput placeholder='Departure date'></TextInput>
        </View>
      </View>
      <View style={{
        borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
        marginRight: '10%',
        flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'
      }}>
        <Image source={require('../../assets/Time.png')} style={{ height: 25, width: 25 }}></Image>
        <View style={{ marginLeft: '3%' }}>
          <TextInput placeholder='Departure time'></TextInput>
        </View>
      </View>
    
    <View>
      <TouchableOpacity>
        <View style={{
          marginTop: '3%', justifyContent: 'center', alignItems: "center",
          borderWidth: 2, marginLeft: '20%', marginRight: '20%', borderRadius: 20,
          backgroundColor: '#ff9999', padding: 4
        }}>
          <Text style={{ fontSize: 20, color: 'black' }}>Request</Text>
        </View>
      </TouchableOpacity>
    </View>
  </View>
  </ScrollView>
  </View>
);

const SecondRoute = () => (
  <View style={{ flex: 1, backgroundColor: '#ffe0cc' }} >
    <View style={{
      borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
      marginRight: '10%',
      flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'
    }}>
      <Image source={require('../../assets/cityscape.jpg')} style={{ height: 25, width: 25 }}></Image>
      <View style={{ marginLeft: '3%' }}>
        <TextInput placeholder='From City'></TextInput>
      </View>
    </View>
    <View style={{
      borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
      marginRight: '10%',
      flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'
    }}>
      <Image source={require('../../assets/cityscape.jpg')} style={{ height: 25, width: 25 }}></Image>
      <View style={{ marginLeft: '3%' }}>
        <TextInput placeholder='To City'></TextInput>
      </View>
    </View>
    <View style={{
      borderBottomColor: 'gray', borderBottomWidth: 2, marginLeft: '10%',
      marginRight: '10%',
      flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'
    }}>
      <Image source={require('../../assets/calendar.png')} style={{ height: 25, width: 25 }}></Image>
      <View style={{ marginLeft: '3%' }}>
        <TextInput placeholder='Departure date'></TextInput>
      </View>
    </View>
    <View style={{ marginTop: '10%', justifyContent: 'center', alignItems: 'center' }}>
      <Text style={{fontSize:20}}>Popular Directions</Text>
    </View>

  </View>
);

const ThirdRoute = () => (
  <View style={{
    flex: 1, backgroundColor: '#ffe0cc', justifyContent: 'center',
    alignItems: 'center'
  }} >
    <Text style={{ fontSize: 20 }}>No ride requests</Text>
  </View>
);
export default function Intercity() {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'Find a ride',  },
    { key: 'second', title: 'Ride offers'},
    { key: 'third', title: 'My rides' },
  ]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute,
  });

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{ width: layout.width }}
      renderTabBar={props => <TabBar {...props} style={{ backgroundColor: '#ff9999' }} 
      labelStyle={{fontSize:15}}
      activeColor="black"
      />}
    />

  );
}