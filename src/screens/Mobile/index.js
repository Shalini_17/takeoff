import React from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
const Mobile = ({navigation}) => {
    const Send=()=>{
        navigation.navigate('OTP')
    }
    return (
        <View style={{ backgroundColor:'#ffe0cc', height:'100%', width:'100%'}}>
            <View style={{marginTop:'30%', justifyContent: 'center', alignItems: 'flex-start',
             borderWidth:2, borderRadius:10, marginLeft:'20%', marginRight:'20%' }}>
                <TextInput placeholder='Enter Mobile Number' placeholderTextColor='black' keyboardType="numeric"></TextInput>
            </View>
            <TouchableOpacity onPress={()=>Send()}>
                <View style={{marginTop:'10%', justifyContent: 'center', alignItems: "center",
                borderWidth:2, marginLeft:'40%', marginRight:'40%', borderRadius:10, backgroundColor:'#ff9999',padding:6 }}>
                    <Text style={{fontSize:20, color:'black'}}>Send</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}
export default Mobile;