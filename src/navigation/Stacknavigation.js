import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
import Login from '../screens/Login';
import Splash from '../screens/Splash';
import Home from '../screens/Home';
import Mobile from '../screens/Mobile';
import OTP from '../screens/OTP';
import Intercity from '../screens/Intercity';
const MainStack =()=>{
    return(
    <Stack.Navigator>
        <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}}/>
        <Stack.Screen name="Login" component={Login} options={{headerShown:false}}/>
        <Stack.Screen name="Home" component={Home} options={{headerShown:false}}/>
        <Stack.Screen name="Mobile" component={Mobile} options={{headerShown:false}}/>
        <Stack.Screen name="OTP" component={OTP} options={{headerShown:false}}/>
        <Stack.Screen name="Intercity" component={Intercity}/>
    </Stack.Navigator>
    );
}
export default MainStack;